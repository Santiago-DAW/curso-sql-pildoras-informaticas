select * from productos
where seccion='deportes' or seccion='ceramica'
order by seccion; -- ORDER BY siempre debe de ir al final.

select * from productos
where seccion='deportes' or seccion='ceramica'
order by seccion desc -- orden descendente (de mayor a menor).

select * from productos
where seccion='deportes' or seccion='ceramica'
order by precio;

select * from productos
where seccion='depoertes' or seccion='ceramica'
order by seccion, precio; -- primero ordena por seccion, y dentro de cada 
-- seccion, ordena por precio.

select * from productos
where seccion='deportes' or seccion='ceramica'
order by seccion, precio desc; -- primero ordena por seccion, y dentro de 
-- cada seccion, ordena por precio descendente (de mayor a menor).

select * from productos
where seccion='deportes' or seccion='ceramica'
order by seccion, paisDeOrigen;

select * from productos
where seccion='deportes' or seccion='ceramica'
order by seccion, paisDeOrigen, precio; -- primero ordena por seccion, dentro 
-- de cada seccion ordena por paisDeOrigen, y dentro de cada paisDeOrigen 
-- ordena por precio.