-- Consultas de acción 2:

delete from clientes;
-- borra todos los registros de la tabla clientes.

delete from clientes
where poblacion='madrid';

delete from productos
where seccion='deportivos'
and precio between 50 and 100;

delete from productos
where seccion='deportivos' or seccion='ceramica';

select empresa from clientes
inner join pedidos
on clientes.codigoCliente=pedidos.codigoCliente;
-- clientes que han hecho pedidos.

-- Predicados:
-- distinct - lo ponemos delante de un campo, muestra valores únicos 
-- (muestra solo una copia de los valores repetidos y no repetidos).
select distinct empresa from clientes
inner join pedidos
on clientes.codigoCliente=pedidos.codigoCliente;
-- clientes que han hecho pedidos (sin repetir listado).

-- distinctrow - no muestra registros repetidos. Mira toda la fila, 
-- todo el registro.
select * from productos
where seccion='ferreteria';
-- en ésta consulta hay dos registros iguales (repetidos).

select distinctrow * from productos
where seccion='ferreteria';
-- elimina registros duplicados y se queda solo con una copia, 
-- también muestra los registros no duplicados.

delete distinctrow clientes.*, pedidos.codigoCliente
from clientes
left join pedidos
on clientes.codigoCliente=pedidos.codigoCliente
where pedidos.codigoCliente is null;
-- elimina clientes que no hayan hecho pedidos.
