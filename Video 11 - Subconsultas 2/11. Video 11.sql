-- Subconsultas
-- Predicado IN: están en.
select nombreArticulo, precio from productos
where codigoArticulo in (
  select codigoArticulo from productosPedidos
  where unidades > 20
);

select nombreArticulo, precio from productos
inner join productosPedidos
on productos.codigoArticulo=productosPedidos.codigoArticulo
where unidades>20;

-- Predicado NOT IN: no están en.
-- Clientes que no han hecho pedidos o que han hecho pedidos pero 
-- NO han pagado con tarjeta.
select empresa, poblacion from clientes
where codigoCliente
not in (
  select codigoCliente from pedidos
  where formaDePago='tarjeta'
);
-- también aparecen los clientes que no han hecho pedidos porque 
-- no están en la tabla de pedidos.
