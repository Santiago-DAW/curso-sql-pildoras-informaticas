-- Consultas de cálculo
-- Se realizan sobre registros individuales, no sobre grupos como 
-- las consultas de agrupación o totales.
-- Es frecuente que éstas consultas de cálculo se utilizen funciones 
-- sql (frecuente pero no necesario).
-- Las funciones se salen del estandar sql porque cada gestor de 
-- BBDD tiene las suyas propias, pero si que es cierto que hay 
-- unas cuantas funciones que suelen ser comunes.
--    • Now() - devuelve el día y la hora actuales.
--    • Datediff() - devuelve la diferencia que hay entre dos fechas.
--    • Date_format() - nos permiten formatear el formato de una 
--                fecha.
--    • format() - nos permite formatear los resultados, quitar 
--                decimales.
--    • round() - permite redondear un valor numérico.
--    • truncate()
--    • concat() - concatena, se suele utilizar con cadenas de texto.

-- Las consultas de cálculo nos permiten por ejemplo crear una 
-- consulta en la cual el precio aparezca con un 21% incrementado 
-- del IVA.
-- Podemos calcular con campos de tipo numérico, fecha, moneda, etc.

-- Creamos un campo nuevo, con el cálculo, QUE NO EXISTE EN LA 
-- TABLA.
select nombreArticulo, seccion, precio, precio+(precio*0.21) 
select nombreArticulo, seccion, precio, precio*1.21
from productos;

select nombreArticulo, seccion, precio, precio*1.21 as precio_con_iva
from productos;

select nombreArticulo, seccion, precio, round(precio*1.21, 2) 
as precio_con_iva
from roductos;

select nombreArticulo, seccion, precio, precio-3 as precio_dto
from productos;

select nombreArticulo, seccion, precio, fecha
from productos
where seccion='deportes';

select nombreArticulo, seccion, precio, fecha, now() as fecha_de_hoy
from productos
where seccion='deportes';

select nombreArticulo, seccion, precio, fecha,
now() as fecha_de_hoy, datediff(now(), fecha)
from productos
where seccion='deportes';

select nombreArticulo, seccion, precio, fecha,
now() as fecha_de_hoy, datediff(now(), fecha) as diferencia_fechas
from productos
where seccion='deportes';

select nombreArticulo, seccion, precio, fecha,
date_format(now(), '%D-%M') as fecha_de_hoy,
datediff(now(), fecha) as diferencia_de_fechas
from productos
where seccion='deportes';
