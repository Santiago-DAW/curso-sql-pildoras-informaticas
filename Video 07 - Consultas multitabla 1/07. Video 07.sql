-- Consultas multitabla - consultas con Union
-- Requisitos:
--    • Deben tener el mismo número de campos.
--    • Los campos deben de tener tipos de datos compatibles.
--          (campo numérico y campo moneda son compatibles).
--    • Puede ocurrir que los campos en las dos tablas tengan 
--          nombres diferentes.
--    • (Mismo orden en campos de tabla 1 y tabla 2, mismo número
--          de campos y los campos deben tener tipo de datos 
--          compatibles).

-- Cuando se fusionan las tablas, los campos toman el nombre de la 
-- tabla 1.

select * from productos
where seccion='deportes'
union
select * from productosNuevos
where seccion='deportes de riesgo';
-- A la hora de establecer criterios (where), no tienen que ser 
-- criterios basados en el mismo campo. Podemos establecer criteio 
-- precio en la primera consulta y paisDeOrigen en la segunda 
-- consulta.

select * from productos
where precio>500
union
select * from productosNuevos
where seccion='alta costura';

-- Union: en el caso de que haya registros repetidos en tabla 1
-- o tabla 2, esos registros repetidos los muestra solo una vez.
-- UnionAll: cuando hay registros repetidos en las tablas de unión 
-- tabla 1 y tabla 2, esos registros repetidos los muestra siempre 
-- que aparecen.

select * from productos
union
select * from productosNuevos; -- no muestra registros duplicados.
-- Utiliza el registro de la tabla 1.

select * from productos
union all
select * from productosNuevos; -- muestra todos los resgistros de 
-- las dos tablas (muestra registros repetidos y no repetidos, se 
-- repitan una vez como docientas).
