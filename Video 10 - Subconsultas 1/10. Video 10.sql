-- Subconsultas:

select avg(precio) from productos;
-- Subconsulta escalonada:
-- devuelve un único registro y un único campo.
-- a esto se le conoce como subconsulta escalonada.

select nombreArticulo, seccion from productos
where precio > (select avg(precio) from productos);

select nombreArticulo, seccion from productos
where precio < (select avg(precio) from productos);

-- Subconsulta de lista:
-- devuelve una lista de registros.
-- se utiliza mucho con los operadores IN, ANY y ALL.
select precio from productos
where seccion='ceramica';

select * from productos
where precio 
> all
(select precio from productos
where seccion='ceramica');
-- ALL: indica que el precio sea mayor a todos los precios de 
-- de la seccion de cerámica.

-- All <-- todos          max
-- Any <-- cualquiera     min   (> || > || > ... del listado).

select * from productos
where precio
> Any
(select precio from productos
where seccion='ceramica');
-- devuelve los productos mayor que cualquiera del los productos 
-- que pueda devolvernos la subconsulta.

select precio from productos
where seccion='jugueteria';

select * from productos
where precio
> all
(select precio from productos
where seccion='jugueteria');
