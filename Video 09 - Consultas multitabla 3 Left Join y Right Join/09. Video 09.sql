-- Left Join:

-- Access:
select * from clientes Left join pedidos
on clientes.[codigo cliente]=pedidos.[codigo cliente]
where poblacion='madrid';
-- Muestra los clientes que han hecho pedidos y los que no.


-- MySql:
select clientes.codigoCliente, poblacion, direccion, 
numeroPedido, pedidos.codigoCliente, formaDePago
from clientes inner join pedidos
on clientes.codigoCliente=pedidos.codigoCliente;
-- muestra todos los pedidos que han hecho los clientes.

select clientes.codigoCliente, poblacion, direccion, 
numeroPedido, pedidos.codigoCliente, formaDePago
from clientes inner join pedidos
on clientes.codigoCliente=pedidos.codigoCliente
where poblacion='madrid';

select clientes.codigoCliente, poblacion, direccion,
numeroPedido, pedidos.codigoCliente, formaDePago
from clientes left join pedidos
on clientes.codigoCliente=pedidos.codigoCliente
where poblacion='madrid';
-- muestra todos los clientes de madrid (aunque no hayan hecho 
-- pedidos) y los pedidos de los clientes (clientes con pedidos).

select clientes.codigoCliente, poblacion, direccion,
numeroPedido, pedidos.codigoCliente, formaDePago
from clientes left join pedidos
on clientes.codigoCliente=pedidos.codigoCliente
where poblacion='madrid' and pedidos.codigoCliente is null;
-- muestra aquellos clientes de madrid que no han hecho pedidos.


-- Right Join:
-- Muestra todos los pedidos, incluso aquellos que no tengan
-- cliente (es no se va a dar por la naturaleza de nuestra BBDD,
-- porque no podemos tener clientes en pedidos que no hayan hecho 
-- pedidos --integridad referencial--).
