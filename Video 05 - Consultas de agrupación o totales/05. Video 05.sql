-- Consultas de agrupación o totales
-- Estas consultas lo que hacen es hacer cálculos por grupos.
-- Para realizar estas consultas necesitamos dos campos:
--    • campo de agrupación
--    • campo del cálculo

select seccion, sum(precio) from productos
group by seccion; -- agrupa los regristros por sección y una vez 
-- los tiene agrupados, coge esos registros y suma sus precios.

select seccion, sum(precio) from productos
group by seccion
order by precio; -- MAL, no existe ningún campo llamado precio en 
-- la tabla resultado (sum(precio)) y la cláusula order by lo que 
-- hace es buscar el campo precio en la consulta (no en la tabla 
-- resultado).

select seccion, sum(precio) as suma_articulos from productos
group by seccion
order by suma_articulos; -- BIEN. Añadimos un alias con "as".

-- Si añadimos un tercer campo, depende del sistema gestor de BBDD 
-- hará distintas cosas, en este caso añade el primer artículo de 
-- cada sección.
select seccion, nombreArticulo, sum(precio) as suma_articulos
from productos
group by seccion
order by suma_articulos;

select seccion, avg(precio) as media_articulos from productos
group by seccion
where seccion='deportes' or seccion='confeccion'; -- MAL.

select seccion, avg(precio) as media_articulos from productos
group by seccion
having seccion='deportes' or seccion='confeccion'; -- BIEN.
-- having sustituye al where en las consultas de agrupación.

select seccion, avg(precio) as media_articulos from productos
group by seccion
having seccion='deportes' or seccion='confeccion'
order by media_articulos;

-- La función count() NO CUENTA REGISTROS EN BLANCO.
select poblacion, count(codigoCliente) as num_clientes
from clientes
group by poblacion;

select seccion, max(precio) as precio_mayor
from productos
where seccion='confeccion'
group by seccion;

select seccion, nombreArticulo, max(precio) as precio_mayor
from productos
where seccion='confeccion'
group by seccion; -- MAL, en nombre que me da no es del artículo 
-- más caro de confección, me da el nombre del primer artículo de 
-- la sección de confección, no el del más caro.


