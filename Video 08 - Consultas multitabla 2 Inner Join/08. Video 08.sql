-- Inner Join:
-- Access

select * from clientes inner join pedidos
on clientes.codigoCliente=pedidos.codigoCliente;
-- tablas relacionadas por campo codigoCliente.
-- consulta muestra todos los clientes que han hecho pedidos.

select * from clientes inner join pedidos
on clientes.codigoCliente=pedidos.codigoCliente
where poblacion='madrid';

select * from clientes inner join pedidos
on clientes.[codigo cliente]=pedidos.[codigo cliente]
where poblacion='madrid';

-- estas consultas incluyen los nombres de todos los campos de las 
-- dos tablas.
-- Solo nos muestra los registros que están en ambas tablas, en 
-- función de una columna relacionada entre las tablas.
