-- Datos anexados:
-- Tenemos 2 tablas con el mismo número de campos y con el mismo 
-- tipo de datos, pero los registros son diferentes.
-- Permite anexar (unir) todos los registros o parte de los 
-- registros de una tabla en otra.

insert into clientes
select * from clientes_madrid;

delete from clientes
where poblacion='madrid';

insert into clientes (codigoCliente, empresa, poblacion, telefono)
select codigoCliente, empresa, poblacion, telefono
from clientes_madrid;
-- tener cuuidado con los campos clave y con los campos requeridos.

delete * from clientes
where poblacion='madrid';

insert into (empresa, direccion, poblacion)
select empresa, direccion, poblacion
from clientes_madrid;
-- la consulta da un error porque estamos ignorando el campo 
-- codigoCliente (campo clave requerido).