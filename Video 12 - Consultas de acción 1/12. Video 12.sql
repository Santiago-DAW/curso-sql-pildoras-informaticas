-- Consultas de acción
-- Actualización:
update productos set precio=precio+10;
-- actualiza el precio de todos los productos.

update productos set precio=precio+10
where seccion='deportes';

update productos set precio=precio+10
where seccion='deportes';

update productos set seccion='deportivos';
-- actualiza el nombre de la sección de todos los artículos de la 
-- tabla.

update productos set seccion='deportivos'
where seccion='deportes';

-- ----------------------------------------------------
-- Creación:
-- Creamos una tabla a partir de otra (Access).
select * into clientes_madrid from clientes;
-- creamos una copia de la tabla de clientes.

select * into clientes_madrid from clientes
where poblacion='madrid';

-- MySql
create table clientes_madrid select * from clientes;
-- creamos una copia de la tabla de clientes.

create table clientes_madrid select * from clientes
where poblacion='madrid';

-- select into: inseta una serie de datos en una tabla, sin 
-- determinar propiedades y características de esa tabla.
-- create table: es más potente, permite estableces qué claves 
-- va a tener la nueva tabla o propiedad los campos.
