-- Referencias cruzadas:

-- Access
transform sum(precio) as total  -- zona de totales
select nombreArticulo           -- campo zona filas
from productos                  -- campo zona filas
group by nombreArticulo         -- campo zona filas
pivot seccion;                  -- campo zona columnas
-- hay dos destornilladores y nos muestra la suma de los dos.

transform sum(precio)
select seccion
from productos
group by seccion
pivot nombreArticulo;

transform count(precio) as num_articulos
select nombreArticulo
from productos
group by nombreArticulo
pivot seccion;

transform count(codigoArticulo) as num_articulos
select nombreArticulo
from productos
group by nombreArticulo
pivot seccion;

-- 2 tablas
-- 1ro consulta seleccion - campos a visualizar
select empresa, poblacion, formaDePago --(formaDePago - pedidos)
from clientes
inner join pedidos
on clientes.codigoCliente=pedidos.codigoCliente;

transform count(poblacion) as total_formaPago
select empresa
from
  select empresa, poblacion, formaDePago
  from clientes
  inner join productos
  on clientes.codigoCliente=pedidos.codigoCliente
group by empresa
pivot formaDePago;
